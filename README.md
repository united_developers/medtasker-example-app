# MedTasker Example App
Application for convenient distribution of tasks.

Technology Stack: VSCode, Flutter, Dart, Provider, Firebase, Intl.

Created by Daniil Belikov.

# Preview:
<img src="./preview/preview.png"/>
