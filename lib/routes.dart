///
///  routes.dart
///  MedTasker
///
///  Created by Daniil Belikov on 14.09.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'helpers/auth_manager.dart';
import 'screens/sign_in_screen.dart';
import 'screens/sign_up_screen.dart';
import 'screens/add_task_screen.dart';
import 'package:flutter/material.dart';
import 'screens/read_task_screen.dart';
import 'screens/edit_task_screen.dart';

final Map<String, WidgetBuilder> routes = {
  '/': (context) => AuthManager(),
  '/login': (context) => SignInScreen(),
  '/register': (context) => SignUpScreen(),
  '/addtask': (context) => AddTaskScreen(),
  '/readtask': (context) => ReadTaskScreen(),
  '/edittask': (context) => EditTaskScreen(),
};
