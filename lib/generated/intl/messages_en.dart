// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("About"),
        "alertFour": MessageLookupByLibrary.simpleMessage(
            "No such user exists. Check the entered data."),
        "alertOne": MessageLookupByLibrary.simpleMessage(
            "This user is already registered."),
        "alertThree": MessageLookupByLibrary.simpleMessage(
            "Wrong password. Check the entered data."),
        "alertTwo": MessageLookupByLibrary.simpleMessage(
            "Something went wrong. Please try to register later."),
        "back": MessageLookupByLibrary.simpleMessage("Back"),
        "buttonFinish": MessageLookupByLibrary.simpleMessage("To complete"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "chooseDate": MessageLookupByLibrary.simpleMessage("Choose a date"),
        "chooseResp": MessageLookupByLibrary.simpleMessage("Select a executor"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "connectionError": MessageLookupByLibrary.simpleMessage(
            "Internet connection problems."),
        "createTask": MessageLookupByLibrary.simpleMessage("Add task"),
        "deadline": MessageLookupByLibrary.simpleMessage("Deadline:"),
        "deadlineT": MessageLookupByLibrary.simpleMessage("Deadline"),
        "description": MessageLookupByLibrary.simpleMessage(
            "A positive and executive specialist. I am interested in mobile development and English. I have experience in publishing applications in the App Store and Google Play, as well as experience in localizing applications in several languages. In my free time I study undergraduate degree in Applied Informatics, and also launch my own startups."),
        "descriptionT":
            MessageLookupByLibrary.simpleMessage("Description of the task"),
        "developer": MessageLookupByLibrary.simpleMessage("Daniil Belikov"),
        "done": MessageLookupByLibrary.simpleMessage("Done"),
        "edit": MessageLookupByLibrary.simpleMessage("Editing"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "emailRules":
            MessageLookupByLibrary.simpleMessage("Mail must contain @."),
        "employee": MessageLookupByLibrary.simpleMessage("Nick Harper"),
        "emptyFields": MessageLookupByLibrary.simpleMessage(
            "All text fields must be filled in."),
        "enter": MessageLookupByLibrary.simpleMessage("Login"),
        "enterEmail": MessageLookupByLibrary.simpleMessage("Enter your email"),
        "enterError": MessageLookupByLibrary.simpleMessage("Login failed"),
        "enterName":
            MessageLookupByLibrary.simpleMessage("Enter first and last name"),
        "enterPassword": MessageLookupByLibrary.simpleMessage("Enter password"),
        "entrance": MessageLookupByLibrary.simpleMessage("Login"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "errorT": MessageLookupByLibrary.simpleMessage(
            "The text must be more than 3 characters."),
        "finish": MessageLookupByLibrary.simpleMessage("Completed"),
        "hire": MessageLookupByLibrary.simpleMessage("Hire"),
        "inProcess": MessageLookupByLibrary.simpleMessage("During"),
        "information": MessageLookupByLibrary.simpleMessage("Task details"),
        "loading": MessageLookupByLibrary.simpleMessage("Loading"),
        "logout": MessageLookupByLibrary.simpleMessage("Logout"),
        "nameRule": MessageLookupByLibrary.simpleMessage(
            "The name must contain more than one character."),
        "noAccount":
            MessageLookupByLibrary.simpleMessage("Don\'t have an account?"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "passwordMatch":
            MessageLookupByLibrary.simpleMessage("Password mismatch."),
        "passwordRepeat":
            MessageLookupByLibrary.simpleMessage("Repeat password"),
        "passwordRule": MessageLookupByLibrary.simpleMessage(
            "Password must be more than 5 characters."),
        "platform": MessageLookupByLibrary.simpleMessage("Flutter Developer"),
        "publication": MessageLookupByLibrary.simpleMessage("Publish"),
        "registration": MessageLookupByLibrary.simpleMessage("Register?"),
        "registrationButton":
            MessageLookupByLibrary.simpleMessage("Register now"),
        "registrationTitle":
            MessageLookupByLibrary.simpleMessage("Registration"),
        "rememberMe": MessageLookupByLibrary.simpleMessage("Remember me"),
        "responsible": MessageLookupByLibrary.simpleMessage("Executor:"),
        "responsibleT": MessageLookupByLibrary.simpleMessage("Executor"),
        "saveButton": MessageLookupByLibrary.simpleMessage("Save"),
        "show": MessageLookupByLibrary.simpleMessage("More details"),
        "status": MessageLookupByLibrary.simpleMessage("Status:"),
        "sure": MessageLookupByLibrary.simpleMessage("Are you sure?"),
        "sureDescription": MessageLookupByLibrary.simpleMessage(
            "The task will be permanently deleted from the system."),
        "tasks": MessageLookupByLibrary.simpleMessage("Tasks"),
        "titleT": MessageLookupByLibrary.simpleMessage("Title"),
        "updateButton": MessageLookupByLibrary.simpleMessage("Refresh"),
        "yes": MessageLookupByLibrary.simpleMessage("Ok")
      };
}
