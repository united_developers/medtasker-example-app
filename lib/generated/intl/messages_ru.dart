// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("Информация"),
        "alertFour": MessageLookupByLibrary.simpleMessage(
            "Такого пользователя не существует. Проверьте введенные данные."),
        "alertOne": MessageLookupByLibrary.simpleMessage(
            "Такой пользователь уже зарегистрирован."),
        "alertThree": MessageLookupByLibrary.simpleMessage(
            "Неверный пароль. Проверьте введенные данные."),
        "alertTwo": MessageLookupByLibrary.simpleMessage(
            "Что-то пошло не так. Попробуйте зарегистрироваться позже."),
        "back": MessageLookupByLibrary.simpleMessage("Назад"),
        "buttonFinish": MessageLookupByLibrary.simpleMessage("Завершить"),
        "cancel": MessageLookupByLibrary.simpleMessage("Отмена"),
        "chooseDate": MessageLookupByLibrary.simpleMessage("Выберите дату"),
        "chooseResp":
            MessageLookupByLibrary.simpleMessage("Выберите исполнителя"),
        "close": MessageLookupByLibrary.simpleMessage("Закрыть"),
        "connectionError": MessageLookupByLibrary.simpleMessage(
            "Проблемы с подключением к интернету."),
        "createTask": MessageLookupByLibrary.simpleMessage("Добавить задачу"),
        "deadline": MessageLookupByLibrary.simpleMessage("Срок:"),
        "deadlineT": MessageLookupByLibrary.simpleMessage("Срок сдачи"),
        "description": MessageLookupByLibrary.simpleMessage(
            "Позитивный и достаточно исполнительный специалист. Интересуюсь мобильной разработкой и английским языком. Имею опыт публикации приложений в App Store и Google Play, а также опыт локализации приложений на нескольких языках. В свободное время учусь на бакалавриате по направлению «Прикладная информатика», а также запускю собственные стартапы."),
        "descriptionT": MessageLookupByLibrary.simpleMessage("Описание задачи"),
        "developer": MessageLookupByLibrary.simpleMessage("Даниил Беликов"),
        "done": MessageLookupByLibrary.simpleMessage("Готово"),
        "edit": MessageLookupByLibrary.simpleMessage("Редактирование"),
        "email": MessageLookupByLibrary.simpleMessage("Электронная почта"),
        "emailRules":
            MessageLookupByLibrary.simpleMessage("Почта должна содержать @."),
        "employee": MessageLookupByLibrary.simpleMessage("Илья Пономарёв"),
        "emptyFields": MessageLookupByLibrary.simpleMessage(
            "Все текстовые поля должны быть заполнены."),
        "enter": MessageLookupByLibrary.simpleMessage("Войти"),
        "enterEmail":
            MessageLookupByLibrary.simpleMessage("Введите электронную почту"),
        "enterError": MessageLookupByLibrary.simpleMessage("Ошибка входа"),
        "enterName":
            MessageLookupByLibrary.simpleMessage("Введите имя и фамилию"),
        "enterPassword": MessageLookupByLibrary.simpleMessage("Введите пароль"),
        "entrance": MessageLookupByLibrary.simpleMessage("Вход"),
        "error": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "errorT": MessageLookupByLibrary.simpleMessage(
            "Текст должен быть больше 3-х символов."),
        "finish": MessageLookupByLibrary.simpleMessage("Завершено"),
        "hire": MessageLookupByLibrary.simpleMessage("Нанять"),
        "inProcess": MessageLookupByLibrary.simpleMessage("В процессе"),
        "information": MessageLookupByLibrary.simpleMessage("Детали задачи"),
        "loading": MessageLookupByLibrary.simpleMessage("Загрузка..."),
        "logout": MessageLookupByLibrary.simpleMessage("Выйти"),
        "nameRule": MessageLookupByLibrary.simpleMessage(
            "Имя должно содержать более одного символа."),
        "noAccount": MessageLookupByLibrary.simpleMessage("Нет аккаунта?"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "passwordMatch":
            MessageLookupByLibrary.simpleMessage("Пароли не совпадают."),
        "passwordRepeat":
            MessageLookupByLibrary.simpleMessage("Повторите пароль"),
        "passwordRule": MessageLookupByLibrary.simpleMessage(
            "Пароль должен содержать более 5 символов."),
        "platform": MessageLookupByLibrary.simpleMessage("Flutter разработчик"),
        "publication": MessageLookupByLibrary.simpleMessage("Опубликовать"),
        "registration":
            MessageLookupByLibrary.simpleMessage("Зарегистрироваться?"),
        "registrationButton":
            MessageLookupByLibrary.simpleMessage("Зарегистрироваться"),
        "registrationTitle":
            MessageLookupByLibrary.simpleMessage("Регистрация"),
        "rememberMe": MessageLookupByLibrary.simpleMessage("Запомнить меня"),
        "responsible": MessageLookupByLibrary.simpleMessage("Исполнитель:"),
        "responsibleT": MessageLookupByLibrary.simpleMessage("Исполнитель"),
        "saveButton": MessageLookupByLibrary.simpleMessage("Сохранить"),
        "show": MessageLookupByLibrary.simpleMessage("Подробнее"),
        "status": MessageLookupByLibrary.simpleMessage("Статус:"),
        "sure": MessageLookupByLibrary.simpleMessage("Вы уверены?"),
        "sureDescription": MessageLookupByLibrary.simpleMessage(
            "Задача будет удалена из системы без возможности восстановления."),
        "tasks": MessageLookupByLibrary.simpleMessage("Задачи"),
        "titleT": MessageLookupByLibrary.simpleMessage("Заголовок"),
        "updateButton": MessageLookupByLibrary.simpleMessage("Обновить"),
        "yes": MessageLookupByLibrary.simpleMessage("Да")
      };
}
