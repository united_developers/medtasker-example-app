// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Tasks`
  String get tasks {
    return Intl.message(
      'Tasks',
      name: 'tasks',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get about {
    return Intl.message(
      'About',
      name: 'about',
      desc: '',
      args: [],
    );
  }

  /// `Task details`
  String get information {
    return Intl.message(
      'Task details',
      name: 'information',
      desc: '',
      args: [],
    );
  }

  /// `Add task`
  String get createTask {
    return Intl.message(
      'Add task',
      name: 'createTask',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get entrance {
    return Intl.message(
      'Login',
      name: 'entrance',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get enter {
    return Intl.message(
      'Login',
      name: 'enter',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get error {
    return Intl.message(
      'Error',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  /// `Executor:`
  String get responsible {
    return Intl.message(
      'Executor:',
      name: 'responsible',
      desc: '',
      args: [],
    );
  }

  /// `Deadline:`
  String get deadline {
    return Intl.message(
      'Deadline:',
      name: 'deadline',
      desc: '',
      args: [],
    );
  }

  /// `Status:`
  String get status {
    return Intl.message(
      'Status:',
      name: 'status',
      desc: '',
      args: [],
    );
  }

  /// `During`
  String get inProcess {
    return Intl.message(
      'During',
      name: 'inProcess',
      desc: '',
      args: [],
    );
  }

  /// `Completed`
  String get finish {
    return Intl.message(
      'Completed',
      name: 'finish',
      desc: '',
      args: [],
    );
  }

  /// `Editing`
  String get edit {
    return Intl.message(
      'Editing',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `To complete`
  String get buttonFinish {
    return Intl.message(
      'To complete',
      name: 'buttonFinish',
      desc: '',
      args: [],
    );
  }

  /// `Enter your email`
  String get enterEmail {
    return Intl.message(
      'Enter your email',
      name: 'enterEmail',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Mail must contain @.`
  String get emailRules {
    return Intl.message(
      'Mail must contain @.',
      name: 'emailRules',
      desc: '',
      args: [],
    );
  }

  /// `Login failed`
  String get enterError {
    return Intl.message(
      'Login failed',
      name: 'enterError',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Enter first and last name`
  String get enterName {
    return Intl.message(
      'Enter first and last name',
      name: 'enterName',
      desc: '',
      args: [],
    );
  }

  /// `The name must contain more than one character.`
  String get nameRule {
    return Intl.message(
      'The name must contain more than one character.',
      name: 'nameRule',
      desc: '',
      args: [],
    );
  }

  /// `Enter password`
  String get enterPassword {
    return Intl.message(
      'Enter password',
      name: 'enterPassword',
      desc: '',
      args: [],
    );
  }

  /// `Password must be more than 5 characters.`
  String get passwordRule {
    return Intl.message(
      'Password must be more than 5 characters.',
      name: 'passwordRule',
      desc: '',
      args: [],
    );
  }

  /// `Password mismatch.`
  String get passwordMatch {
    return Intl.message(
      'Password mismatch.',
      name: 'passwordMatch',
      desc: '',
      args: [],
    );
  }

  /// `Repeat password`
  String get passwordRepeat {
    return Intl.message(
      'Repeat password',
      name: 'passwordRepeat',
      desc: '',
      args: [],
    );
  }

  /// `Remember me`
  String get rememberMe {
    return Intl.message(
      'Remember me',
      name: 'rememberMe',
      desc: '',
      args: [],
    );
  }

  /// `Don't have an account?`
  String get noAccount {
    return Intl.message(
      'Don\'t have an account?',
      name: 'noAccount',
      desc: '',
      args: [],
    );
  }

  /// `Register?`
  String get registration {
    return Intl.message(
      'Register?',
      name: 'registration',
      desc: '',
      args: [],
    );
  }

  /// `Registration`
  String get registrationTitle {
    return Intl.message(
      'Registration',
      name: 'registrationTitle',
      desc: '',
      args: [],
    );
  }

  /// `Register now`
  String get registrationButton {
    return Intl.message(
      'Register now',
      name: 'registrationButton',
      desc: '',
      args: [],
    );
  }

  /// `This user is already registered.`
  String get alertOne {
    return Intl.message(
      'This user is already registered.',
      name: 'alertOne',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong. Please try to register later.`
  String get alertTwo {
    return Intl.message(
      'Something went wrong. Please try to register later.',
      name: 'alertTwo',
      desc: '',
      args: [],
    );
  }

  /// `Wrong password. Check the entered data.`
  String get alertThree {
    return Intl.message(
      'Wrong password. Check the entered data.',
      name: 'alertThree',
      desc: '',
      args: [],
    );
  }

  /// `No such user exists. Check the entered data.`
  String get alertFour {
    return Intl.message(
      'No such user exists. Check the entered data.',
      name: 'alertFour',
      desc: '',
      args: [],
    );
  }

  /// `Internet connection problems.`
  String get connectionError {
    return Intl.message(
      'Internet connection problems.',
      name: 'connectionError',
      desc: '',
      args: [],
    );
  }

  /// `More details`
  String get show {
    return Intl.message(
      'More details',
      name: 'show',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get titleT {
    return Intl.message(
      'Title',
      name: 'titleT',
      desc: '',
      args: [],
    );
  }

  /// `Executor`
  String get responsibleT {
    return Intl.message(
      'Executor',
      name: 'responsibleT',
      desc: '',
      args: [],
    );
  }

  /// `Deadline`
  String get deadlineT {
    return Intl.message(
      'Deadline',
      name: 'deadlineT',
      desc: '',
      args: [],
    );
  }

  /// `The text must be more than 3 characters.`
  String get errorT {
    return Intl.message(
      'The text must be more than 3 characters.',
      name: 'errorT',
      desc: '',
      args: [],
    );
  }

  /// `Description of the task`
  String get descriptionT {
    return Intl.message(
      'Description of the task',
      name: 'descriptionT',
      desc: '',
      args: [],
    );
  }

  /// `Refresh`
  String get updateButton {
    return Intl.message(
      'Refresh',
      name: 'updateButton',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get saveButton {
    return Intl.message(
      'Save',
      name: 'saveButton',
      desc: '',
      args: [],
    );
  }

  /// `Publish`
  String get publication {
    return Intl.message(
      'Publish',
      name: 'publication',
      desc: '',
      args: [],
    );
  }

  /// `Nick Harper`
  String get employee {
    return Intl.message(
      'Nick Harper',
      name: 'employee',
      desc: '',
      args: [],
    );
  }

  /// `Back`
  String get back {
    return Intl.message(
      'Back',
      name: 'back',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get done {
    return Intl.message(
      'Done',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure?`
  String get sure {
    return Intl.message(
      'Are you sure?',
      name: 'sure',
      desc: '',
      args: [],
    );
  }

  /// `The task will be permanently deleted from the system.`
  String get sureDescription {
    return Intl.message(
      'The task will be permanently deleted from the system.',
      name: 'sureDescription',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Ok`
  String get yes {
    return Intl.message(
      'Ok',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `Select a executor`
  String get chooseResp {
    return Intl.message(
      'Select a executor',
      name: 'chooseResp',
      desc: '',
      args: [],
    );
  }

  /// `Choose a date`
  String get chooseDate {
    return Intl.message(
      'Choose a date',
      name: 'chooseDate',
      desc: '',
      args: [],
    );
  }

  /// `All text fields must be filled in.`
  String get emptyFields {
    return Intl.message(
      'All text fields must be filled in.',
      name: 'emptyFields',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Loading`
  String get loading {
    return Intl.message(
      'Loading',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Daniil Belikov`
  String get developer {
    return Intl.message(
      'Daniil Belikov',
      name: 'developer',
      desc: '',
      args: [],
    );
  }

  /// `Flutter Developer`
  String get platform {
    return Intl.message(
      'Flutter Developer',
      name: 'platform',
      desc: '',
      args: [],
    );
  }

  /// `A positive and executive specialist. I am interested in mobile development and English. I have experience in publishing applications in the App Store and Google Play, as well as experience in localizing applications in several languages. In my free time I study undergraduate degree in Applied Informatics, and also launch my own startups.`
  String get description {
    return Intl.message(
      'A positive and executive specialist. I am interested in mobile development and English. I have experience in publishing applications in the App Store and Google Play, as well as experience in localizing applications in several languages. In my free time I study undergraduate degree in Applied Informatics, and also launch my own startups.',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Hire`
  String get hire {
    return Intl.message(
      'Hire',
      name: 'hire',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
