///
///  constants.dart
///  MedTasker
///
///  Created by Daniil Belikov on 12.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';

class TaskerFont {
  static const light = 'GothamProLight';
  static const regular = 'GothamProRegular';
  static const medium = 'GothamProMedium';
  static const bold = 'GothamProBold';
}

class TaskerImages {
  static const logoAndroid = 'assets/logoAndroid.png';
  static const process = 'assets/process.png';
  static const done = 'assets/done.png';
  static const caution = 'assets/caution.png';
  static const avatar = 'assets/avatar.png';
  static const developer = 'assets/developer.jpg';
}

class TaskerColors {
  static const white = Color(0xFFFFFFFF);
  static const transparent = Color(0x00000000);
}

class TaskerKeys {
  static const key = 'key';
  static const name = 'name';
}

class TaskerUID {
  static const adminUID = 'kM0S8fyc7DQEvjLdz89bgFjTAhg2';
}

class TaskerURL {
  static const udev = 'https://udev.dev';
}

class TaskerSettings {
  static BoxDecoration shadow = BoxDecoration(
    boxShadow: <BoxShadow>[
      BoxShadow(
        color: Colors.grey[100]!,
        blurRadius: 4.0,
        spreadRadius: 2.0,
        offset: const Offset(0.0, 0.0),
      ),
    ],
    color: TaskerColors.white,
    borderRadius: BorderRadius.circular(20.0),
  );
}
