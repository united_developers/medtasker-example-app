///
///  color_themes.dart
///  MedTasker
///
///  Created by Daniil Belikov on 12.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class TaskerThemes {
  static final light = ThemeData.light().copyWith(
    canvasColor: Color(0xff252525),
    splashColor: Color(0x00000000),
    selectedRowColor: Color(0x223F95AD),
    unselectedWidgetColor: Color(0xffF4F4F4),
    primaryColor: Color(0xff3F95AD),
    backgroundColor: Color(0xffF6F7FB),
    errorColor: Color(0xffCE3939),
    accentColor: Color(0xffFFFFFF),
    indicatorColor: Color(0xff737373),
    primaryTextTheme: TextTheme(
      bodyText1: TextStyle(
        color: Color(0xff282828),
      ),
      headline5: TextStyle(
        color: Color(0xffF4F4F4),
      ),
      headline6: TextStyle(
        color: Color(0xffE7F0FE),
      ),
    ),
  );

  static final dark = ThemeData.dark().copyWith(
    canvasColor: Color(0xff252525),
    splashColor: Color(0x00000000),
    selectedRowColor: Color(0x223F95AD),
    unselectedWidgetColor: Color(0xffF4F4F4),
    primaryColor: Color(0xff3F95AD),
    backgroundColor: Color(0xffF6F7FB),
    errorColor: Color(0xffCE3939),
    accentColor: Color(0xffFFFFFF),
    indicatorColor: Color(0xff737373),
    primaryTextTheme: TextTheme(
      bodyText1: TextStyle(
        color: Color(0xff282828),
      ),
      headline5: TextStyle(
        color: Color(0xffF4F4F4),
      ),
      headline6: TextStyle(
        color: Color(0xffE7F0FE),
      ),
    ),
  );
}
