///
///  auth_manager.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:medtasker/models/user_model.dart';
import 'package:medtasker/services/auth_service.dart';
import 'package:medtasker/screens/sign_in_screen.dart';
import 'package:medtasker/widgets/common/material_bottom_bar.dart';
import 'package:medtasker/widgets/common/cupertino_bottom_bar.dart';

class AuthManager extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AuthService authService = Provider.of<AuthService>(context);
    return StreamBuilder<UserModel?>(
      stream: authService.user,
      builder: (_, AsyncSnapshot<UserModel?> snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          return snapshot.data == null
              ? SignInScreen()
              : Platform.isAndroid
                  ? MaterialStoreHomePage(snapshot.data)
                  : CupertinoStoreHomePage(snapshot.data);
        } else {
          return Container(
            color: Theme.of(context).backgroundColor,
          );
        }
      },
    );
  }
}
