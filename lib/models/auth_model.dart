///
///  auth_model.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

class AuthModel {
  String? name;
  String? email;
  String? password;
  String? password2;

  AuthModel({
    this.name,
    this.email,
    this.password,
    this.password2,
  });
}
