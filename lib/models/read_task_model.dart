///
///  read_task_model.dart
///  MedTasker
///
///  Created by Daniil Belikov on 14.09.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:medtasker/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ReadTaskModel {
  QueryDocumentSnapshot<Object?>? document;
  UserModel? user;

  ReadTaskModel({
    this.document,
    this.user,
  });
}
