///
///  user_model.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

class UserModel {
  final String? uid;
  final String? name;
  final String? email;

  UserModel(
    this.uid,
    this.name,
    this.email,
  );
}
