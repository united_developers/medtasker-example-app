///
///  sign_in_screen.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:medtasker/models/auth_model.dart';
import 'package:medtasker/services/auth_service.dart';
import 'package:medtasker/widgets/app/medtasker_logo.dart';
import 'package:medtasker/widgets/app/medtasker_title.dart';
import 'package:medtasker/widgets/app/medtasker_alert.dart';
import 'package:medtasker/widgets/app/medtasker_button.dart';
import 'package:medtasker/widgets/app/medtasker_checkbox.dart';
import 'package:medtasker/widgets/app/medtasker_textfield.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  GlobalKey<FormState> signInFormKey = GlobalKey<FormState>();
  AuthModel authModel = AuthModel();

  FocusNode _focusNodeName = FocusNode();
  FocusNode _focusNodePassword = FocusNode();

  bool credVerified = false;
  bool isApiCallProcess = false;
  bool rememberMe = true;
  bool hidePassword = true;

  late Color color;

  bool _validateAndSave() {
    final form = signInFormKey.currentState;

    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _initFocusLogic() {
    _focusNodeName.addListener(() {
      setState(() => color = _focusNodeName.hasFocus
          ? Theme.of(context).selectedRowColor
          : Theme.of(context).unselectedWidgetColor);
    });

    _focusNodePassword.addListener(() {
      setState(() => color = _focusNodePassword.hasFocus
          ? Theme.of(context).selectedRowColor
          : Theme.of(context).unselectedWidgetColor);
    });
  }

  void _onRememberMeChanged(bool? newValue) {
    setState(() {
      rememberMe = newValue ?? true;
    });
  }

  Future<AlertDialog?> _showAlertDialog(String text) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return MedTaskerAlert(
          imagePath: TaskerImages.caution,
          alertTitle: S.of(context).error,
          alertText: text,
        );
      },
    );
  }

  @override
  void initState() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    super.initState();
    _initFocusLogic();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Theme.of(context).accentColor,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onPanDown: (_) => FocusScope.of(context).requestFocus(FocusNode()),
        child: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(
              left: 20.0,
              right: 20.0,
            ),
            child: Form(
              key: signInFormKey,
              child: Column(
                children: <Widget>[
                  MedTaskerLogo(),
                  const SizedBox(
                    height: 20.0,
                  ),
                  MedTaskerTitle(
                    text: S.of(context).entrance,
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  MedTaskerTextField(
                    maxLines: 1,
                    myFocusNode: _focusNodeName,
                    keyboardType: TextInputType.text,
                    onSaved: (input) => authModel.email = input,
                    hintText: S.of(context).email,
                    obscureText: false,
                    textInput: TextInputAction.done,
                    onPressed: () {},
                    validator: (input) =>
                        !input!.contains('@') ? S.of(context).emailRules : null,
                    icon: null,
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  MedTaskerTextField(
                    maxLines: 1,
                    myFocusNode: _focusNodePassword,
                    keyboardType: TextInputType.text,
                    onSaved: (input) => authModel.password = input,
                    textInput: TextInputAction.done,
                    onPressed: () =>
                        setState(() => hidePassword = !hidePassword),
                    validator: (input) =>
                        (credVerified) ? S.of(context).enterError : null,
                    hintText: S.of(context).password,
                    obscureText: hidePassword,
                    icon: Padding(
                      padding: EdgeInsets.only(right: 4.0),
                      child: IconButton(
                        splashColor: Theme.of(context).splashColor,
                        color: Theme.of(context).primaryColor,
                        onPressed: () =>
                            setState(() => hidePassword = !hidePassword),
                        icon: Icon(
                          hidePassword
                              ? Icons.visibility_off
                              : Icons.visibility,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    children: [
                      MedTaskerCheckBox(
                        value: rememberMe,
                        onChanged: _onRememberMeChanged,
                      ),
                      Text(
                        S.of(context).rememberMe,
                        style: TextStyle(
                          color: Theme.of(context).textTheme.bodyText1?.color,
                          fontFamily: TaskerFont.regular,
                          fontSize: 14.0,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  MedTaskerButton(
                    accept: true,
                    state: isApiCallProcess,
                    buttonText: S.of(context).enter,
                    sendFunc: () async {
                      if (_validateAndSave()) {
                        setState(() => isApiCallProcess = true);

                        try {
                          await authService
                              .signInWithEmailAndPassword(
                                authModel.email ?? '',
                                authModel.password ?? '',
                              )
                              .then(
                                (value) => {
                                  setState(
                                    () => isApiCallProcess = false,
                                  ),
                                },
                              );
                        } on FirebaseAuthException catch (_) {
                          setState(() => isApiCallProcess = false);
                          _showAlertDialog(S.of(context).alertThree);
                        } on Exception catch (_) {
                          setState(() => isApiCallProcess = false);
                          _showAlertDialog(S.of(context).alertTwo);
                        }
                      }
                    },
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  SignInLabelWidget(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SignInLabelWidget extends StatefulWidget {
  @override
  _SignInLabelWidgetState createState() => _SignInLabelWidgetState();
}

class _SignInLabelWidgetState extends State<SignInLabelWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Text(
            S.of(context).noAccount,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontFamily: TaskerFont.regular,
              fontSize: 13.0,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 4.0),
          child: GestureDetector(
            onTap: () => Navigator.pushNamed(context, '/register'),
            child: Text(
              S.of(context).registration,
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                decoration: TextDecoration.underline,
                fontFamily: TaskerFont.regular,
                fontSize: 13.0,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
