///
///  edit_task_screen.dart
///  MedTasker
///
///  Created by Daniil Belikov on 21.08.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:medtasker/widgets/app/medtasker_alert.dart';
import 'package:medtasker/widgets/app/medtasker_button.dart';
import 'package:medtasker/widgets/app/medtasker_textfield_white.dart';

class EditTaskScreen extends StatefulWidget {
  @override
  _EditTaskScreenState createState() => _EditTaskScreenState();
}

class _EditTaskScreenState extends State<EditTaskScreen> {
  DocumentSnapshot? _document;

  TextEditingController? _titleController;
  TextEditingController? _respController;
  TextEditingController? _deadlController;
  TextEditingController? _descrController;

  bool _isApiCallProcess = false;

  List<String> _employees = [
    'Nick Harper',
    'Mason Yellow',
    'Noah Green',
    'Michael Gray',
    'Alexander Pink',
    'William Red',
  ];

  Future<List<int>?> _showPicker() {
    return Picker(
      height: 150.0,
      looping: false,
      itemExtent: 38.0,
      hideHeader: true,
      textAlign: TextAlign.center,
      cancelText: S.of(context).back,
      confirmText: S.of(context).done,
      adapter: PickerDataAdapter<String>(pickerdata: _employees),
      cancelTextStyle: TextStyle(
        color: Theme.of(context).primaryColor,
        fontFamily: TaskerFont.medium,
        fontSize: 18.0,
      ),
      confirmTextStyle: TextStyle(
        color: Theme.of(context).primaryColor,
        fontFamily: TaskerFont.medium,
        fontSize: 18.0,
      ),
      textStyle: TextStyle(
        color: Theme.of(context).indicatorColor,
        fontFamily: TaskerFont.medium,
        fontSize: 18.0,
      ),
      selectedTextStyle: TextStyle(
        color: Theme.of(context).canvasColor,
        fontFamily: TaskerFont.medium,
        fontSize: 22.0,
      ),
      title: Container(
        alignment: Alignment.center,
        child: Text(
          S.of(context).chooseResp,
          style: TextStyle(
            fontFamily: TaskerFont.medium,
            fontSize: 20.0,
          ),
        ),
      ),
      onConfirm: (Picker picker, List value) {
        _respController?.text = picker.getSelectedValues().first;
      },
    ).showDialog(context);
  }

  Future<List<int>?> _showDatePicker(BuildContext context) {
    return Picker(
        height: 150.0,
        looping: false,
        itemExtent: 38.0,
        hideHeader: true,
        textAlign: TextAlign.center,
        cancelText: S.of(context).back,
        confirmText: S.of(context).done,
        adapter: DateTimePickerAdapter(
          yearBegin: 2021,
          yearEnd: 2030,
          isNumberMonth: true,
          customColumnType: [2, 1, 0, 3, 4],
        ),
        title: Container(
          alignment: Alignment.center,
          child: Text(
            S.of(context).chooseDate,
            style: TextStyle(
              fontFamily: TaskerFont.medium,
              fontSize: 20.0,
            ),
          ),
        ),
        cancelTextStyle: TextStyle(
          color: Theme.of(context).primaryColor,
          fontFamily: TaskerFont.medium,
          fontSize: 18.0,
        ),
        confirmTextStyle: TextStyle(
          color: Theme.of(context).primaryColor,
          fontFamily: TaskerFont.medium,
          fontSize: 18.0,
        ),
        textStyle: TextStyle(
          color: Theme.of(context).indicatorColor,
          fontFamily: TaskerFont.medium,
          fontSize: 16.0,
        ),
        selectedTextStyle: TextStyle(
          color: Theme.of(context).canvasColor,
          fontFamily: TaskerFont.medium,
          fontSize: 18.0,
        ),
        onConfirm: (Picker picker, List value) {
          DateTime? dateTime = (picker.adapter as DateTimePickerAdapter).value;
          String dateString = DateFormat.yMMMMd('en').format(dateTime!);
          String timeString = DateFormat.jm('en').format(dateTime);
          String finalTime = dateString + ' ' + timeString;
          _deadlController?.text = finalTime;
        }).showDialog(context);
  }

  Future<AlertDialog?> _showAlertDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return MedTaskerAlert(
          imagePath: TaskerImages.caution,
          alertTitle: S.of(context).error,
          alertText: S.of(context).emptyFields,
        );
      },
    );
  }

  @override
  void initState() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    RouteSettings settings = ModalRoute.of(context)!.settings;
    _document = settings.arguments as DocumentSnapshot;
    _titleController = TextEditingController(text: _document?.get('title'));
    _respController =
        TextEditingController(text: _document?.get('responsible'));
    _deadlController = TextEditingController(text: _document?.get('deadline'));
    _descrController =
        TextEditingController(text: _document?.get('description'));
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          S.of(context).edit,
          style: TextStyle(
            color: Theme.of(context).canvasColor,
            fontFamily: TaskerFont.bold,
          ),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 20.0,
            bottom: 20.0,
          ),
          child: Column(
            children: [
              Container(
                decoration: TaskerSettings.shadow,
                child: MedTaskerTextFieldWhite(
                  readOnly: false,
                  onTap: () {},
                  maxLines: 1,
                  controller: _titleController,
                  keyboardType: TextInputType.text,
                  onSaved: (input) {},
                  textInput: TextInputAction.done,
                  hintText: S.of(context).titleT,
                  obscureText: false,
                  onPressed: () {},
                  validator: (input) =>
                      input!.length < 3 ? S.of(context).errorT : null,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                decoration: TaskerSettings.shadow,
                child: MedTaskerTextFieldWhite(
                  readOnly: true,
                  maxLines: 1,
                  onTap: _showPicker,
                  controller: _respController,
                  keyboardType: TextInputType.text,
                  onSaved: (input) {},
                  textInput: TextInputAction.done,
                  onPressed: () {},
                  hintText: S.of(context).responsibleT,
                  obscureText: false,
                  validator: (input) =>
                      input!.length < 3 ? S.of(context).errorT : null,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                decoration: TaskerSettings.shadow,
                child: MedTaskerTextFieldWhite(
                  readOnly: true,
                  maxLines: 1,
                  controller: _deadlController,
                  keyboardType: TextInputType.text,
                  onSaved: (input) {},
                  onTap: () => _showDatePicker(context),
                  textInput: TextInputAction.done,
                  onPressed: () {},
                  hintText: S.of(context).deadlineT,
                  obscureText: false,
                  validator: (input) =>
                      input!.length < 3 ? S.of(context).errorT : null,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                decoration: TaskerSettings.shadow,
                child: MedTaskerTextFieldWhite(
                  readOnly: false,
                  onTap: () {},
                  maxLines: 15,
                  controller: _descrController,
                  keyboardType: TextInputType.text,
                  onSaved: (input) {},
                  textInput: TextInputAction.done,
                  onPressed: () {},
                  hintText: S.of(context).descriptionT,
                  obscureText: false,
                  validator: (input) =>
                      input!.length < 3 ? S.of(context).errorT : null,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                child: MedTaskerButton(
                  state: _isApiCallProcess,
                  accept: true,
                  buttonText: S.of(context).updateButton,
                  sendFunc: () {
                    if (_titleController!.text.isNotEmpty &&
                        _respController!.text.isNotEmpty &&
                        _deadlController!.text.isNotEmpty &&
                        _descrController!.text.isNotEmpty) {
                      setState(() => _isApiCallProcess = true);

                      _document?.reference.update({
                        'title': _titleController?.text,
                        'responsible': _respController?.text,
                        'deadline': _deadlController?.text,
                        'description': _descrController?.text,
                        'preview': _document?.get('preview'),
                        'publishDate': _document?.get('publishDate'),
                        'status': _document?.get('status'),
                      }).whenComplete(() {
                        setState(() => _isApiCallProcess = false);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      });
                    } else {
                      _showAlertDialog(context);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
