///
///  about_screen.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import '../../generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:medtasker/helpers/constants.dart';
import '../widgets/common/flexible_space_bar.dart';
import 'package:medtasker/widgets/app/medtasker_button.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

dynamic _launchURL() async {
  const String url = TaskerURL.udev;

  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url.';
  }
}

class _AboutScreenState extends State<AboutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: NestedScrollView(
        headerSliverBuilder: (_, __) => <Widget>[
          SliverAppBar(
            snap: false,
            pinned: true,
            elevation: 0.0,
            centerTitle: false,
            expandedHeight: 96.0,
            automaticallyImplyLeading: false,
            backgroundColor: Theme.of(context).backgroundColor,
            flexibleSpace: BackgroundFlexibleSpaceBar(
              title: Text(
                S.of(context).about,
                style: TextStyle(
                  fontFamily: TaskerFont.bold,
                  color: Theme.of(context).canvasColor,
                  fontSize: 20.0,
                ),
              ),
              centerTitle: false,
              titlePadding: const EdgeInsets.only(
                left: 20.0,
                bottom: 8.0,
              ),
              background: ClipRect(
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                  ),
                ),
              ),
            ),
          ),
        ],
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 150.0,
                width: 150.0,
                child: ClipOval(
                  child: Image.asset(
                    TaskerImages.developer,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30.0),
                child: Text(
                  S.of(context).developer,
                  style: TextStyle(
                    color: Theme.of(context).canvasColor,
                    fontFamily: TaskerFont.bold,
                    fontSize: 26.0,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 4.0),
                child: Text(
                  S.of(context).platform,
                  style: TextStyle(
                    color: Theme.of(context).indicatorColor,
                    fontFamily: TaskerFont.medium,
                    fontSize: 16.0,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 30.0,
                  left: 20.0,
                  right: 20.0,
                ),
                child: Text(
                  S.of(context).description,
                  style: TextStyle(
                    color: Theme.of(context).canvasColor,
                    fontFamily: TaskerFont.regular,
                    fontSize: 14.0,
                    height: 1.25,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  top: 30.0,
                ),
                child: MedTaskerButton(
                  buttonText: S.of(context).hire,
                  sendFunc: _launchURL,
                  accept: true,
                  state: false,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
