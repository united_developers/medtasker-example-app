///
///  read_task_screen.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:medtasker/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:medtasker/models/read_task_model.dart';
import 'package:medtasker/widgets/app/medtasker_button.dart';
import 'package:medtasker/widgets/app/medtasker_alert_action.dart';

class ReadTaskScreen extends StatelessWidget {
  late final ReadTaskModel model;
  late final DocumentSnapshot document;
  late final UserModel user;

  Future<AlertDialog?> _showAlertDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return MedTaskerAlertAction(
          imagePath: TaskerImages.caution,
          alertTitle: S.of(context).sure,
          alertText: S.of(context).sureDescription,
          onPressed: () {
            document.reference.delete().whenComplete(() {
              Navigator.pop(context);
              Navigator.pop(context);
            });
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    RouteSettings settings = ModalRoute.of(context)!.settings;
    model = settings.arguments as ReadTaskModel;
    document = model.document as DocumentSnapshot;
    user = model.user as UserModel;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          S.of(context).information,
          style: TextStyle(
            color: Theme.of(context).canvasColor,
            fontFamily: TaskerFont.bold,
          ),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        centerTitle: true,
        elevation: 0.0,
      ),
      body: TaskDetailBodyWidget(
        title: document.get('title'),
        publishDate: document.get('publishDate'),
        preview: document.get('preview'),
        description: document.get('description'),
        responsible: document.get('responsible'),
        status: document.get('status'),
        deadline: document.get('deadline'),
        document: document,
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            heroTag: '1',
            elevation: 0.0,
            onPressed: () => Navigator.pushNamed(
              context,
              '/edittask',
              arguments: document,
            ),
            child: const Icon(Icons.edit),
            backgroundColor: Theme.of(context).primaryColor,
          ),
          const SizedBox(
            height: 14.0,
          ),
          FloatingActionButton(
            heroTag: '2',
            elevation: 0.0,
            onPressed: () => _showAlertDialog(context),
            child: const Icon(Icons.delete),
            backgroundColor: Theme.of(context).primaryColor,
          ),
          const SizedBox(
            height: 14.0,
          ),
        ],
      ),
    );
  }
}

class TaskDetailBodyWidget extends StatelessWidget {
  TaskDetailBodyWidget({
    required this.title,
    required this.publishDate,
    required this.preview,
    required this.description,
    required this.responsible,
    required this.status,
    required this.deadline,
    required this.document,
  });

  final String title;
  final String publishDate;
  final String preview;
  final String description;
  final String responsible;
  final bool status;
  final String deadline;
  final DocumentSnapshot document;

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(20.0),
        child: Container(
          decoration: TaskerSettings.shadow,
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TaskDetailTitle(
                title: title,
              ),
              TaskDetailDescription(
                description: description,
              ),
              TaskDetailTextRow(
                title: S.of(context).responsible,
                text: responsible,
              ),
              TaskDetailTextRow(
                title: S.of(context).deadline,
                text: deadline,
              ),
              TaskDetailTextRow(
                title: S.of(context).status,
                text: status ? S.of(context).inProcess : S.of(context).finish,
              ),
              if (status == true)
                Container(
                  margin: EdgeInsets.only(top: 15.0),
                  child: MedTaskerButton(
                    state: false,
                    accept: true,
                    buttonText: S.of(context).buttonFinish,
                    sendFunc: () {
                      document.reference.update({
                        'deadline': deadline,
                        'description': description,
                        'preview': preview,
                        'publishDate': publishDate,
                        'responsible': responsible,
                        'status': false,
                        'title': title,
                      }).whenComplete(() {
                        Navigator.pop(context);
                      });
                    },
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

class TaskDetailTitle extends StatelessWidget {
  const TaskDetailTitle({
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10.0),
      child: Text(
        title,
        style: TextStyle(
          color: Theme.of(context).canvasColor,
          fontFamily: TaskerFont.bold,
          fontSize: 21.0,
          height: 1.25,
        ),
      ),
    );
  }
}

class TaskDetailDescription extends StatelessWidget {
  const TaskDetailDescription({
    required this.description,
  });

  final String description;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        top: 20.0,
        bottom: 24.0,
      ),
      child: Text(
        description,
        style: const TextStyle(
          fontFamily: TaskerFont.regular,
          fontSize: 17.0,
          height: 1.25,
        ),
      ),
    );
  }
}

class TaskDetailTextRow extends StatelessWidget {
  const TaskDetailTextRow({
    required this.title,
    required this.text,
  });

  final String title;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        bottom: 10.0,
      ),
      child: Row(
        children: [
          Text(
            title,
            maxLines: 3,
            style: TextStyle(
              color: Theme.of(context).canvasColor,
              fontFamily: TaskerFont.bold,
              fontSize: 15.0,
            ),
          ),
          const SizedBox(
            width: 6.0,
          ),
          Text(
            text,
            maxLines: 3,
            style: TextStyle(
              color: Theme.of(context).canvasColor,
              fontFamily: TaskerFont.regular,
              fontSize: 15.0,
            ),
          ),
        ],
      ),
    );
  }
}
