///
///  sign_up_screen.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'dart:io';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:localstorage/localstorage.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:medtasker/models/auth_model.dart';
import 'package:medtasker/services/auth_service.dart';
import 'package:medtasker/widgets/app/medtasker_logo.dart';
import 'package:medtasker/widgets/app/medtasker_title.dart';
import 'package:medtasker/widgets/app/medtasker_alert.dart';
import 'package:medtasker/widgets/app/medtasker_button.dart';
import 'package:medtasker/widgets/app/medtasker_textfield.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen();

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  GlobalKey<FormState> signUpFormKey = GlobalKey<FormState>();
  AuthModel authModel = AuthModel();

  FocusNode _focusNodeName = FocusNode();
  FocusNode _focusNodeEmail = FocusNode();
  FocusNode _focusNodePassword = FocusNode();
  FocusNode _focusNodePassword2 = FocusNode();

  bool credVerified = false;
  bool isApiCallProcess = false;
  bool hidePassword = true;

  late Color color;

  bool _validateAndSave() {
    final form = signUpFormKey.currentState;

    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _initFocusLogic() {
    _focusNodeName.addListener(() {
      setState(() => color = _focusNodeName.hasFocus
          ? Theme.of(context).selectedRowColor
          : Theme.of(context).unselectedWidgetColor);
    });

    _focusNodeEmail.addListener(() {
      setState(() => color = _focusNodeEmail.hasFocus
          ? Theme.of(context).selectedRowColor
          : Theme.of(context).unselectedWidgetColor);
    });

    _focusNodePassword.addListener(() {
      setState(() => color = _focusNodePassword.hasFocus
          ? Theme.of(context).selectedRowColor
          : Theme.of(context).unselectedWidgetColor);
    });

    _focusNodePassword2.addListener(() {
      setState(() => color = _focusNodePassword.hasFocus
          ? Theme.of(context).selectedRowColor
          : Theme.of(context).unselectedWidgetColor);
    });
  }

  Future<AlertDialog?> _showAlertDialog(String text) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return MedTaskerAlert(
          imagePath: TaskerImages.caution,
          alertTitle: S.of(context).error,
          alertText: text,
        );
      },
    );
  }

  void _save(String name) async {
    final LocalStorage storage = LocalStorage(TaskerKeys.key);
    storage.setItem(TaskerKeys.name, name);
  }

  @override
  void initState() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    super.initState();
    _initFocusLogic();
  }

  @override
  Widget build(BuildContext context) {
    final AuthService authService = Provider.of<AuthService>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Theme.of(context).accentColor,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onPanDown: (_) => FocusScope.of(context).requestFocus(FocusNode()),
        child: Stack(
          children: [
            Container(
              child: const IconBackWidget(),
              margin: const EdgeInsets.only(
                top: 50.0,
                left: 4.0,
              ),
            ),
            Center(
              child: SingleChildScrollView(
                padding: const EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                ),
                child: Form(
                  key: signUpFormKey,
                  child: Column(
                    children: <Widget>[
                      MedTaskerLogo(),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MedTaskerTitle(
                        text: S.of(context).registrationTitle,
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MedTaskerTextField(
                        maxLines: 1,
                        myFocusNode: _focusNodeName,
                        keyboardType: TextInputType.text,
                        onSaved: (input) => authModel.name = input,
                        hintText: S.of(context).enterName,
                        obscureText: false,
                        onPressed: () {},
                        textInput: TextInputAction.done,
                        validator: (input) =>
                            input!.length < 2 ? S.of(context).nameRule : null,
                        icon: null,
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MedTaskerTextField(
                        maxLines: 1,
                        myFocusNode: _focusNodeEmail,
                        keyboardType: TextInputType.text,
                        onSaved: (input) => authModel.email = input,
                        hintText: S.of(context).enterEmail,
                        obscureText: false,
                        onPressed: () {},
                        textInput: TextInputAction.done,
                        validator: (input) => !input!.contains('@')
                            ? S.of(context).emailRules
                            : null,
                        icon: null,
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MedTaskerTextField(
                        maxLines: 1,
                        myFocusNode: _focusNodePassword,
                        keyboardType: TextInputType.text,
                        onSaved: (input) => authModel.password = input,
                        hintText: S.of(context).enterPassword,
                        obscureText: hidePassword,
                        onPressed: () {},
                        textInput: TextInputAction.done,
                        validator: (input) => input!.length < 6
                            ? S.of(context).passwordRule
                            : null,
                        icon: null,
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MedTaskerTextField(
                        maxLines: 1,
                        myFocusNode: _focusNodePassword2,
                        keyboardType: TextInputType.text,
                        onSaved: (input) => authModel.password2 = input,
                        validator: (input) =>
                            (credVerified) ? S.of(context).passwordMatch : null,
                        onPressed: () {},
                        textInput: TextInputAction.done,
                        hintText: S.of(context).passwordRepeat,
                        obscureText: hidePassword,
                        icon: null,
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MedTaskerButton(
                        accept: true,
                        state: isApiCallProcess,
                        buttonText: S.of(context).registrationButton,
                        sendFunc: () async {
                          setState(() => credVerified = false);

                          if (_validateAndSave()) {
                            if (authModel.password == authModel.password2) {
                              setState(() => isApiCallProcess = true);

                              try {
                                await authService
                                    .createUserWithEmailAndPassword(
                                  authModel.name ?? '',
                                  authModel.email ?? '',
                                  authModel.password ?? '',
                                );

                                setState(() => isApiCallProcess = false);
                                _save(authModel.name ?? '');
                                Navigator.pop(context);
                              } on FirebaseAuthException catch (_) {
                                setState(() => isApiCallProcess = false);
                                _showAlertDialog(S.of(context).alertOne);
                              } on Exception catch (_) {
                                setState(() => isApiCallProcess = false);
                                _showAlertDialog(S.of(context).alertTwo);
                              }
                            } else {
                              setState(() => credVerified = true);
                              _validateAndSave();
                            }
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class IconBackWidget extends StatelessWidget {
  const IconBackWidget();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios,
        color: Theme.of(context).primaryColor,
      ),
      onPressed: () => Navigator.pop(context),
    );
  }
}
