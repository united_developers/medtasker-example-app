///
///  tasks_screen.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import '../../generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:medtasker/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:medtasker/models/read_task_model.dart';
import 'package:medtasker/widgets/common/left_side_menu.dart';
import 'package:medtasker/widgets/app/medtasker_post_row.dart';
import 'package:medtasker/widgets/app/medtasker_post_text.dart';
import 'package:medtasker/widgets/app/medtasker_post_title.dart';
import 'package:medtasker/widgets/common/flexible_space_bar.dart';
import 'package:medtasker/widgets/app/medtasker_post_big_row.dart';
import 'package:medtasker/widgets/app/medtasker_activity_indicator.dart';

class TasksScreen extends StatefulWidget {
  const TasksScreen(this.user);

  final UserModel? user;

  @override
  _TasksScreenState createState() => _TasksScreenState();
}

class _TasksScreenState extends State<TasksScreen> {
  final _ref = FirebaseFirestore.instance
      .collection('tasks')
      .orderBy('publishDate', descending: true);

  UserModel? get user => widget.user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: LeftSideMenu(user),
      backgroundColor: Theme.of(context).backgroundColor,
      body: NestedScrollView(
        headerSliverBuilder: (_, __) => <Widget>[
          SliverAppBar(
            snap: false,
            pinned: true,
            elevation: 0.0,
            centerTitle: false,
            expandedHeight: 96.0,
            automaticallyImplyLeading: false,
            backgroundColor: Theme.of(context).backgroundColor,
            actions: <Widget>[
              Container(
                child: Builder(
                  builder: (context) => IconButton(
                    onPressed: () => Scaffold.of(context).openDrawer(),
                    icon: Icon(
                      Icons.menu,
                      color: Theme.of(context).primaryColor,
                      size: 30.0,
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(right: 16.0),
                child: IconButton(
                  onPressed: () => Navigator.pushNamed(context, '/addtask'),
                  icon: Icon(
                    Icons.add_task,
                    color: Theme.of(context).primaryColor,
                    size: 26.0,
                  ),
                ),
              ),
            ],
            flexibleSpace: BackgroundFlexibleSpaceBar(
              title: Text(
                S.of(context).tasks,
                style: TextStyle(
                  color: Theme.of(context).canvasColor,
                  fontFamily: TaskerFont.bold,
                  fontSize: 20.0,
                ),
              ),
              centerTitle: false,
              titlePadding: const EdgeInsets.only(
                left: 20.0,
                bottom: 8.0,
              ),
              background: ClipRect(
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                  ),
                ),
              ),
            ),
          ),
        ],
        body: TasksBodyWidget(
          decoration: TaskerSettings.shadow,
          fireContainer: _ref,
          user: user,
        ),
      ),
    );
  }
}

class TasksBodyWidget extends StatelessWidget {
  const TasksBodyWidget({
    required this.fireContainer,
    required this.decoration,
    required this.user,
  });

  final Query fireContainer;
  final BoxDecoration decoration;
  final UserModel? user;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: fireContainer.snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData)
          return Scrollbar(
            child: ListView.builder(
              padding: const EdgeInsets.only(
                top: 10.0,
                bottom: 10.0,
                left: 20.0,
                right: 20.0,
              ),
              itemCount: snapshot.hasData ? snapshot.data?.docs.length : 0,
              itemBuilder: (context, index) {
                return SingleChildScrollView(
                  padding: const EdgeInsets.only(
                    top: 18.0,
                    bottom: 8.0,
                  ),
                  child: InkWell(
                    highlightColor: Colors.transparent,
                    onTap: () {
                      ReadTaskModel arguments = ReadTaskModel(
                        document: snapshot.data!.docs[index],
                        user: user,
                      );
                      Navigator.pushNamed(
                        context,
                        '/readtask',
                        arguments: arguments,
                      );
                    },
                    child: Container(
                      padding: const EdgeInsets.all(20.0),
                      decoration: decoration,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          MedTaskerPostTitle(
                            snapshot: snapshot,
                            index: index,
                          ),
                          MedTaskerPostText(
                            snapshot: snapshot,
                            index: index,
                          ),
                          MedTaskerPostRow(
                            snapshot: snapshot,
                            title: S.of(context).responsible,
                            text: snapshot.data?.docs[index].get('responsible'),
                            index: index,
                          ),
                          MedTaskerPostRow(
                            snapshot: snapshot,
                            title: S.of(context).deadline,
                            text: snapshot.data?.docs[index].get('deadline'),
                            index: index,
                          ),
                          MedTaskerPostBigRow(
                            snapshot: snapshot,
                            title: S.of(context).status,
                            status: snapshot.data?.docs[index].get('status'),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        return const MedTaskerActivityIndicator();
      },
    );
  }
}
