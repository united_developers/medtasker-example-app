///
///  med_tasker_app.dart
///  MedTasker
///
///  Created by Daniil Belikov on 12.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'generated/l10n.dart';
import 'services/auth_service.dart';
import 'package:medtasker/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:medtasker/helpers/color_themes.dart';
import 'package:medtasker/widgets/common/noglow_scroll.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class MedTaskerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AuthService>(
          create: (_) => AuthService(),
        ),
      ],
      child: AdaptiveTheme(
        initial: AdaptiveThemeMode.light,
        light: TaskerThemes.light,
        dark: TaskerThemes.dark,
        builder: (light, dark) => MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: light,
          darkTheme: dark,
          scrollBehavior: NoGlowScrollBehavior(),
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          initialRoute: '/',
          routes: routes,
        ),
      ),
    );
  }
}
