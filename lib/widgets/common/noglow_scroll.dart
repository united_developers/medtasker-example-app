///
///  material_bottom_bar.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.08.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';

class NoGlowScrollBehavior extends MaterialScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
    BuildContext context,
    Widget child,
    ScrollableDetails details,
  ) {
    return child;
  }
}
