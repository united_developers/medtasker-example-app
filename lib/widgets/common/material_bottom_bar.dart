///
///  material_bottom_bar.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:medtasker/models/user_model.dart';
import 'package:medtasker/screens/tasks_screen.dart';
import 'package:medtasker/screens/about_screen.dart';

class MaterialStoreHomePage extends StatefulWidget {
  const MaterialStoreHomePage(this.user);

  final UserModel? user;

  @override
  _MaterialStoreHomePageState createState() => _MaterialStoreHomePageState();
}

class _MaterialStoreHomePageState extends State<MaterialStoreHomePage> {
  PageController pageController = PageController();
  UserModel? get user => widget.user;

  int _selectedIndex = 0;

  void _onPageChanged(int index) {
    setState(() => _selectedIndex = index);
  }

  void _onItemTapped(int index) {
    pageController.jumpToPage(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: pageController,
        onPageChanged: _onPageChanged,
        children: [
          TasksScreen(user),
          AboutScreen(),
        ],
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedLabelStyle: TextStyle(
          fontFamily: TaskerFont.medium,
        ),
        unselectedLabelStyle: TextStyle(
          fontFamily: TaskerFont.medium,
        ),
        unselectedItemColor: Theme.of(context).indicatorColor,
        backgroundColor: Theme.of(context).backgroundColor,
        selectedItemColor: Theme.of(context).primaryColor,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: S.of(context).tasks,
            icon: Icon(
              Icons.event,
              size: 26.0,
            ),
          ),
          BottomNavigationBarItem(
            label: S.of(context).about,
            icon: Icon(
              Icons.info_outline,
              size: 26.0,
            ),
          ),
        ],
      ),
    );
  }
}
