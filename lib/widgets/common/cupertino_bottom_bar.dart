///
///  cupertino_bottom_bar.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/models/user_model.dart';
import 'package:medtasker/screens/tasks_screen.dart';
import 'package:medtasker/screens/about_screen.dart';

class CupertinoStoreHomePage extends StatefulWidget {
  const CupertinoStoreHomePage(this.user);

  final UserModel? user;

  @override
  _CupertinoPageState createState() => _CupertinoPageState();
}

class _CupertinoPageState extends State<CupertinoStoreHomePage> {
  UserModel? get user => widget.user;

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: CupertinoTabScaffold(
        resizeToAvoidBottomInset: false,
        tabBar: CupertinoTabBar(
          backgroundColor: Theme.of(context).backgroundColor,
          inactiveColor: Theme.of(context).indicatorColor,
          activeColor: Theme.of(context).primaryColor,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              label: S.of(context).tasks,
              icon: Icon(
                Icons.event,
                size: 26.0,
              ),
            ),
            BottomNavigationBarItem(
              label: S.of(context).about,
              icon: Icon(
                Icons.info_outline,
                size: 26.0,
              ),
            ),
          ],
        ),
        tabBuilder: (BuildContext context, index) {
          return [
            TasksScreen(user),
            AboutScreen(),
          ][index];
        },
      ),
    );
  }
}
