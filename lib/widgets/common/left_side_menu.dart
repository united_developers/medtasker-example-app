///
///  left_side_menu.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:localstorage/localstorage.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:medtasker/models/user_model.dart';
import 'package:medtasker/services/auth_service.dart';
import 'package:medtasker/widgets/app/medtasker_title.dart';

class LeftSideMenu extends StatelessWidget {
  LeftSideMenu(this.user);

  final LocalStorage storage = LocalStorage(TaskerKeys.key);
  final UserModel? user;

  @override
  Widget build(BuildContext context) {
    final String name = user?.name ?? storage.getItem(TaskerKeys.name);
    final String email = user?.email ?? S.of(context).loading;
    final AuthService authService = Provider.of<AuthService>(context);
    return Drawer(
      child: Container(
        color: Theme.of(context).backgroundColor,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(
                name,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1?.color,
                  fontFamily: TaskerFont.medium,
                  fontSize: 16.0,
                ),
              ),
              accountEmail: Text(
                email,
                style: TextStyle(
                  color: Theme.of(context).indicatorColor,
                  fontFamily: TaskerFont.light,
                  fontSize: 14.0,
                ),
              ),
              currentAccountPicture: ClipOval(
                child: Image.asset(
                  TaskerImages.avatar,
                  fit: BoxFit.cover,
                  width: 90.0,
                  height: 90.0,
                ),
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.exit_to_app,
                color: Theme.of(context).primaryColor,
                size: 27.0,
              ),
              title: MedTaskerTitle(
                text: S.of(context).logout,
              ),
              onTap: () async => await authService.signOut(),
            ),
          ],
        ),
      ),
    );
  }
}
