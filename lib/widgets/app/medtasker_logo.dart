///
///  medtasker_logo.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/helpers/constants.dart';

class MedTaskerLogo extends StatelessWidget {
  const MedTaskerLogo();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset(TaskerImages.logoAndroid),
      width: double.infinity,
      height: 150.0,
    );
  }
}
