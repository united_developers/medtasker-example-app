///
///  medtasker_post_text.dart
///  MedTasker
///
///  Created by Daniil Belikov on 21.08.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MedTaskerPostText extends StatelessWidget {
  const MedTaskerPostText({
    required this.snapshot,
    required this.index,
  });

  final AsyncSnapshot<QuerySnapshot> snapshot;
  final int index;

  @override
  Widget build(BuildContext context) {
    String preview = snapshot.data?.docs[index].get('preview');
    return Container(
      padding: const EdgeInsets.only(
        top: 20.0,
        bottom: 16.0,
      ),
      child: Text(
        preview,
        style: const TextStyle(
          fontFamily: TaskerFont.regular,
          fontSize: 17.0,
          height: 1.25,
        ),
      ),
    );
  }
}
