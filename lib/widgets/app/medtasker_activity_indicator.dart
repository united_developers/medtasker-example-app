///
///  medtasker_activity_indicator.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class MedTaskerActivityIndicator extends StatelessWidget {
  const MedTaskerActivityIndicator();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Center(
        child: Platform.isAndroid
            ? CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).primaryColor,
                ),
              )
            : const CupertinoActivityIndicator(radius: 16.0),
      ),
    );
  }
}
