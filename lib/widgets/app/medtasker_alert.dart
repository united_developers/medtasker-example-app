///
///  medtasker_alert.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:medtasker/widgets/app/medtasker_button.dart';

class MedTaskerAlert extends StatelessWidget {
  const MedTaskerAlert({
    required this.imagePath,
    required this.alertTitle,
    required this.alertText,
  });

  final String imagePath;
  final String alertTitle;
  final String alertText;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(0.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      content: Container(
        width: 312.0,
        height: 351.0,
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 34.0,
            ),
            Container(
              height: 69.0,
              width: double.infinity,
              child: Image.asset(imagePath),
            ),
            const SizedBox(
              height: 22.0,
            ),
            Text(
              alertTitle,
              style: TextStyle(
                fontFamily: TaskerFont.bold,
                fontSize: 22.0,
              ),
            ),
            const SizedBox(
              height: 40.0,
            ),
            Center(
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                  left: 31.0,
                  right: 31.0,
                ),
                child: Text(
                  alertText,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  style: TextStyle(
                    fontFamily: TaskerFont.medium,
                    fontSize: 17.0,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 50.0,
            ),
            Container(
              margin: EdgeInsets.only(
                left: 30.0,
                right: 30.0,
              ),
              child: MedTaskerButton(
                state: false,
                accept: true,
                buttonText: S.of(context).close,
                sendFunc: () => Navigator.pop(context),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
