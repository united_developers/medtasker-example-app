///
///  medtasker_post_row.dart
///  MedTasker
///
///  Created by Daniil Belikov on 21.08.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MedTaskerPostRow extends StatelessWidget {
  const MedTaskerPostRow({
    required this.snapshot,
    required this.title,
    required this.text,
    required this.index,
  });

  final AsyncSnapshot<QuerySnapshot> snapshot;
  final String title;
  final String text;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 10.0,
      ),
      child: Row(
        children: [
          Text(
            title,
            maxLines: 3,
            style: TextStyle(
              color: Theme.of(context).canvasColor,
              fontFamily: TaskerFont.bold,
              fontSize: 15.0,
            ),
          ),
          SizedBox(
            width: 6.0,
          ),
          Text(
            text,
            maxLines: 3,
            style: TextStyle(
              color: Theme.of(context).canvasColor,
              fontFamily: TaskerFont.regular,
              fontSize: 15.0,
            ),
          ),
        ],
      ),
    );
  }
}
