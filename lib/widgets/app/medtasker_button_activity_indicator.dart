///
///  medtasker_button_activity_indicator.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';

class ButtonActivityIndicator extends StatelessWidget {
  const ButtonActivityIndicator();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20.0,
      width: 20.0,
      child: Center(
        child: CircularProgressIndicator(
          strokeWidth: 2.0,
          valueColor: AlwaysStoppedAnimation<Color>(
            Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}
