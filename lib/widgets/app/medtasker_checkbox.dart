///
///  medtasker_checkbox.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';

class MedTaskerCheckBox extends StatelessWidget {
  const MedTaskerCheckBox({
    required this.value,
    required this.onChanged,
  });

  final bool value;
  final Function(bool?) onChanged;

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 1.3,
      child: Theme(
        data: ThemeData(
          unselectedWidgetColor: Theme.of(context).primaryColor,
        ),
        child: Checkbox(
          activeColor: Theme.of(context).primaryColor,
          checkColor: Theme.of(context).accentColor,
          onChanged: onChanged,
          value: value,
        ),
      ),
    );
  }
}
