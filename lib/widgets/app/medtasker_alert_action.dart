///
///  medtasker_alert_action.dart
///  MedTasker
///
///  Created by Daniil Belikov on 16.09.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:medtasker/widgets/app/medtasker_button.dart';

class MedTaskerAlertAction extends StatelessWidget {
  const MedTaskerAlertAction({
    required this.imagePath,
    required this.alertTitle,
    required this.alertText,
    required this.onPressed,
  });

  final void Function()? onPressed;

  final String imagePath;
  final String alertTitle;
  final String alertText;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(0.0),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      content: Container(
        width: 312.0,
        height: 404.0,
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 34.0,
            ),
            Container(
              height: 69.0,
              width: double.infinity,
              child: Image.asset(imagePath),
            ),
            const SizedBox(
              height: 22.0,
            ),
            Text(
              alertTitle,
              style: TextStyle(
                fontFamily: TaskerFont.bold,
                fontSize: 22.0,
              ),
            ),
            const SizedBox(
              height: 40.0,
            ),
            Center(
              child: Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(
                  left: 31.0,
                  right: 31.0,
                ),
                child: Text(
                  alertText,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  style: const TextStyle(
                    fontFamily: TaskerFont.medium,
                    fontSize: 17.0,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 40.0,
            ),
            Container(
              margin: const EdgeInsets.only(
                left: 30.0,
                right: 30.0,
              ),
              child: Column(
                children: [
                  MedTaskerButton(
                    state: false,
                    accept: true,
                    buttonText: S.of(context).yes,
                    sendFunc: onPressed!,
                  ),
                  const SizedBox(
                    width: 10.0,
                  ),
                  MedTaskerButton(
                    state: false,
                    accept: true,
                    buttonText: S.of(context).cancel,
                    sendFunc: () => Navigator.pop(context),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
