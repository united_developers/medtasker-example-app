///
///  medtasker_textfield.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:medtasker/helpers/constants.dart';

class MedTaskerTextField extends StatelessWidget {
  const MedTaskerTextField({
    required this.validator,
    required this.onPressed,
    required this.onSaved,
    required this.maxLines,
    required this.keyboardType,
    required this.myFocusNode,
    required this.obscureText,
    required this.hintText,
    required this.icon,
    required this.textInput,
  });

  final String? Function(String?)? validator;
  final Function() onPressed;
  final Function(String?)? onSaved;
  final TextInputType keyboardType;
  final FocusNode myFocusNode;
  final bool obscureText;
  final int maxLines;
  final String hintText;
  final Widget? icon;
  final TextInputAction? textInput;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: maxLines,
      autocorrect: false,
      focusNode: myFocusNode,
      keyboardAppearance: Brightness.light,
      textCapitalization: TextCapitalization.none,
      textInputAction: textInput,
      cursorColor: Theme.of(context).primaryColor,
      style: TextStyle(
        color: Theme.of(context).primaryTextTheme.bodyText1?.color,
        fontFamily: TaskerFont.regular,
        fontSize: 16.0,
      ),
      onSaved: onSaved,
      validator: validator,
      obscureText: obscureText,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        filled: true,
        hintText: hintText,
        fillColor: myFocusNode.hasFocus
            ? Theme.of(context).selectedRowColor
            : Theme.of(context).unselectedWidgetColor,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).splashColor,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).splashColor,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).splashColor,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).errorColor,
            width: 2.0,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).errorColor,
            width: 2.0,
          ),
        ),
        suffixIcon: icon,
      ),
    );
  }
}
