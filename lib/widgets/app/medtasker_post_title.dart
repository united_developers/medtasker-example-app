///
///  medtasker_post_title.dart
///  MedTasker
///
///  Created by Daniil Belikov on 21.08.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MedTaskerPostTitle extends StatelessWidget {
  const MedTaskerPostTitle({
    required this.snapshot,
    required this.index,
  });

  final AsyncSnapshot<QuerySnapshot> snapshot;
  final int index;

  @override
  Widget build(BuildContext context) {
    final String title = snapshot.data?.docs[index].get('title');
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(
        title,
        style: TextStyle(
          color: Theme.of(context).canvasColor,
          fontFamily: TaskerFont.bold,
          fontSize: 21.0,
          height: 1.25,
        ),
      ),
    );
  }
}
