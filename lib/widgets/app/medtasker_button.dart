///
///  medtasker_button.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'medtasker_button_activity_indicator.dart';
import 'package:medtasker/helpers/constants.dart';

class MedTaskerButton extends StatelessWidget {
  const MedTaskerButton({
    required this.state,
    required this.accept,
    required this.buttonText,
    required this.sendFunc,
  });

  final bool state;
  final bool accept;
  final String buttonText;
  final Function() sendFunc;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10.0),
      height: 50.0,
      width: double.infinity,
      decoration: TaskerSettings.shadow,
      child: Platform.isAndroid
          ? ElevatedButton(
              onPressed: accept ? sendFunc : null,
              style: ElevatedButton.styleFrom(
                primary: Theme.of(context).primaryColor,
                onPrimary: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),
                ),
              ),
              child: state
                  ? const ButtonActivityIndicator()
                  : Text(
                      buttonText,
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontFamily: TaskerFont.medium,
                        fontSize: 17.0,
                      ),
                    ),
            )
          : CupertinoButton(
              borderRadius: BorderRadius.circular(16.0),
              onPressed: accept ? sendFunc : null,
              color: Theme.of(context).primaryColor,
              child: state
                  ? const ButtonActivityIndicator()
                  : Text(
                      buttonText,
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontFamily: TaskerFont.medium,
                        letterSpacing: 0.3,
                        fontSize: 16.0,
                      ),
                    ),
            ),
    );
  }
}
