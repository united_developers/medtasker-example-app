///
///  medtasker_title.dart
///  MedTasker
///
///  Created by Daniil Belikov on 31.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/helpers/constants.dart';

class MedTaskerTitle extends StatelessWidget {
  const MedTaskerTitle({
    required this.text,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: Theme.of(context).primaryColor,
        fontFamily: TaskerFont.bold,
        fontSize: 18.0,
      ),
    );
  }
}
