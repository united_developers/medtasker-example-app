///
///  medtasker_textfield_white.dart
///  MedTasker
///
///  Created by Daniil Belikov on 21.08.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:medtasker/helpers/constants.dart';

class MedTaskerTextFieldWhite extends StatelessWidget {
  const MedTaskerTextFieldWhite({
    required this.controller,
    required this.validator,
    required this.onPressed,
    required this.onSaved,
    required this.maxLines,
    required this.keyboardType,
    required this.obscureText,
    required this.hintText,
    required this.textInput,
    required this.onTap,
    required this.readOnly,
  });

  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final Function() onPressed;
  final Function(String?)? onSaved;
  final TextInputType keyboardType;
  final bool obscureText;
  final int maxLines;
  final String hintText;
  final TextInputAction? textInput;
  final Function() onTap;
  final bool readOnly;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      maxLines: maxLines,
      textInputAction: textInput,
      autocorrect: true,
      keyboardAppearance: Brightness.light,
      textCapitalization: TextCapitalization.sentences,
      cursorColor: Theme.of(context).primaryColor,
      style: TextStyle(
        color: Theme.of(context).primaryTextTheme.bodyText1?.color,
        fontFamily: TaskerFont.regular,
        fontSize: 16.0,
        height: 1.25,
      ),
      readOnly: readOnly,
      onTap: onTap,
      onSaved: onSaved,
      validator: validator,
      obscureText: obscureText,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        filled: true,
        hintText: hintText,
        fillColor: Theme.of(context).accentColor,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).splashColor,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).splashColor,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).splashColor,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).errorColor,
            width: 2.0,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          ),
          borderSide: BorderSide(
            color: Theme.of(context).errorColor,
            width: 2.0,
          ),
        ),
      ),
    );
  }
}
