///
///  medtasker_post_big_row.dart
///  MedTasker
///
///  Created by Daniil Belikov on 17.08.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/generated/l10n.dart';
import 'package:medtasker/helpers/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MedTaskerPostBigRow extends StatelessWidget {
  const MedTaskerPostBigRow({
    required this.snapshot,
    required this.title,
    required this.status,
  });

  final AsyncSnapshot<QuerySnapshot> snapshot;
  final String title;
  final bool status;

  @override
  Widget build(BuildContext context) {
    final String text = status ? S.of(context).inProcess : S.of(context).finish;
    return Container(
      margin: const EdgeInsets.only(
        left: 0.0,
        right: 0.0,
        top: 0.0,
        bottom: 0.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 40.0,
            child: Row(
              children: [
                Text(
                  title,
                  maxLines: 3,
                  style: TextStyle(
                    color: Theme.of(context).canvasColor,
                    fontFamily: TaskerFont.bold,
                    fontSize: 15.0,
                  ),
                ),
                const SizedBox(
                  width: 6.0,
                ),
                Text(
                  text,
                  maxLines: 3,
                  style: TextStyle(
                    color: Theme.of(context).canvasColor,
                    fontFamily: TaskerFont.regular,
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 40.0,
            padding: EdgeInsets.all(4.0),
            child: Row(
              children: [
                status
                    ? Image.asset(TaskerImages.process)
                    : Image.asset(TaskerImages.done),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
