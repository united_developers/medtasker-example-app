///
///  main.dart
///  MedTasker
///
///  Created by Daniil Belikov on 12.07.2021.
///  Copyright © 2021 United Developers. All rights reserved.
///

import 'package:flutter/material.dart';
import 'package:medtasker/med_tasker_app.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  runApp(MedTaskerApp());
}
